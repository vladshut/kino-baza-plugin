chrome.runtime.sendMessage({todo: "showPageAction"});

const $filmBox = $('.details-panel');
const $filmTitle = $('h2.altname');
const $filmYear = $('h1.title__movie > a');
const $poster = $('.poster img');

if ($filmBox.length > 0 && $filmTitle.length > 0 && $filmYear.length > 0) {
    $filmBox.after(`
    <div class="row" style="margin: 80px 0 50px 0; max-height: 700px; height: 555px">
        <div id="yohoho"
            data-title="${$filmTitle.text()} (${$filmYear.text()})"
            data-titleOrig="${$filmTitle.text()}"
            data-year="${$filmYear.text()}"
            data-poster="${$poster.attr('src')}">      
        </div>
    </div>`);

    ahoy_yo();
}