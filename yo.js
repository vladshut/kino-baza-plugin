const baseUrl = 'https://switch-case.com.ua';
let isTabSelected = false;
let tolokaTabInited = false;

function ahoy_yo() {
    var a = document.querySelectorAll('[data-ahoy]');
    if (a && a.length) {
        for (var i in a) {
            if (a.hasOwnProperty(i) && a[i]) {
                a[i].addEventListener('click', function () {
                    yo(this.getAttribute('data-ahoy'));
                });
            }
        }
    } else {
        yo();
    }
}

function yo(sel) {
    var h, a, w, i, l, y, s, t = false, p = '';

    y = document.querySelector('#' + ((sel) ? sel : 'yohoho'));
    if (!y) {
        y = document.querySelector('#yohoho-online');
        if (!y) {
            y = document.querySelector('#yohoho-torrent');
            if (!y) {
                return false;
            } else {
                t = true;
            }
        }
    }

    var yohoho = document.createElement('div');
    var attr = Array.prototype.slice.call(y.attributes);
    while (a = attr.pop()) {
        yohoho.setAttribute(a.nodeName, a.nodeValue);
    }
    yohoho.innerHTML = y.innerHTML;
    y.parentNode.replaceChild(yohoho, y);

    var options = [].slice.call(yohoho.attributes).reduce(function (o, a) {
        return /^data-/.test(a.name) && (o[a.name.substr(5)] = decodeURIComponent(a.value)), o;
    }, {});

    if ((options.title && /трейлер|trailer|teaser/i.test(options.title)) || t) {
        options.player = 'trailer';
    }

    options.player = ((options.title && /трейлер|trailer|teaser/i.test(options.title)) || t)
        ? 'trailer'
        : (!options.player)
            ? 'moonwalk,hdgo,hdbaza,kodik,czx.to,allserials,trailer,torrent'
            : options.player;

    var bg = (options.bg && options.bg.replace(/[^0-9a-z]/ig, ''))
        ? options.bg.replace(/[^0-9a-z]/ig, '')
        : 'd4d4d4';

    var language = (options.language && /en/i.test(options.language))
        ? {"trailer": "TRAILER", "torrent": "DOWNLOAD"}
        : {"trailer": "ТРЕЙЛЕР", "torrent": "ЗАВАНТАЖИТИ"};

    var btns = {};
    options.button = (options.button)
        ? options.button
        : 'moonwalk: {Q} {T}, hdgo: {Q} {T}, hdbaza: {Q} {T}, kodik: {Q} {T}, iframe: {Q} {T}';
    if (options.button) {
        options.button.split(',').forEach(function (button) {
            var btn = button.split(':');
            if (btn.length === 2 && btn[0] && btn[1]) {
                btns[btn[0].trim().toLowerCase()] = btn[1].trim();
            }
        });
    }

    for (var data in options) {
        if (options.hasOwnProperty(data) && options[data]) {
            p += (p)
                ? '&' + data + '=' + encodeURIComponent(options[data])
                : data + '=' + encodeURIComponent(options[data]);
        } else {
            options[data] = '';
        }
    }

    if (!options.kinopoisk && !options.title) {
        return false;
    }

    var yohoho_loading = document.querySelector('#yohoho-loading');
    if (yohoho_loading) {
        yohoho_loading.parentNode.removeChild(yohoho_loading);
    }
    var yohoho_buttons = document.querySelector('#yohoho-buttons');
    if (yohoho_buttons) {
        yohoho_buttons.parentNode.removeChild(yohoho_buttons);
    }
    var yohoho_iframe = document.querySelector('#yohoho-iframe');
    if (yohoho_iframe) {
        yohoho_iframe.parentNode.removeChild(yohoho_iframe);
    }
    var data_ahoy = document.querySelectorAll('[data-ahoy]');
    for (var da in data_ahoy) {
        if (data_ahoy.hasOwnProperty(da) && data_ahoy[da]) {
            var yohoho_da = document.querySelector('#' + data_ahoy[da].getAttribute('data-ahoy'));
            if (yohoho_da) {
                yohoho_da.removeAttribute('style');
            }
        }
    }

    var head = document.head || document.getElementsByTagName('head')[0];
    var css = `
        #yohoho-loading {
            z-index:9999;
            position:absolute;
            left:0;
            top:0;
            width:100%;
            height:100%;
            background:#${bg} url(data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMTIwIiBoZWlnaHQ9IjEyMCIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB2aWV3Qm94PSIwIDAgMTAwIDEwMCIgcHJlc2VydmVBc3BlY3RSYXRpbz0ieE1pZFlNaWQiIGNsYXNzPSJ1aWwtc3BpcmFsIj48cmVjdCB3aWR0aD0iMTAwIiBoZWlnaHQ9IjEwMCIgY2xhc3M9ImJrIiBmaWxsPSJub25lIi8+PHBhdGggZD0iTTU0LjUgODkuOWMtOS42IDAtMTguNi0zLjktMjUuNC0xMSAtNi44LTcuMS0xMC41LTE2LjYtMTAuNS0yNi43IDAtOC45IDMuMy0xNy4yIDkuMi0yMy41UzQxLjcgMTkgNTAuMiAxOWM4LjQgMCAxNi40IDMuNCAyMi4zIDkuNyA2IDYuMyA5LjIgMTQuNiA5LjIgMjMuNSAwIDE1LjgtMTIuMiAyOC43LTI3LjMgMjguNyAtMTUgMC0yNy4zLTEyLjktMjcuMy0yOC43IDAtMTMuMyAxMC4zLTI0LjIgMjMtMjQuMnMyMyAxMC44IDIzIDI0LjJjMCAxMC44LTguNCAxOS42LTE4LjcgMTkuNiAtMTAuMyAwLTE4LjctOC44LTE4LjctMTkuNiAwLTguMyA2LjUtMTUuMSAxNC40LTE1LjEgNy45IDAgMTQuNCA2LjggMTQuNCAxNS4xIDAgNS44LTQuNSAxMC42LTEwLjEgMTAuNnMtMTAuMS00LjgtMTAuMS0xMC42YzAtMy40IDIuNi02LjEgNS44LTYuMSAzLjIgMCA1LjggMi43IDUuOCA2LjEgMCAwLjktMC43IDEuNi0xLjUgMS42IC0wLjggMC0xLjUtMC43LTEuNS0xLjYgMC0xLjYtMS4zLTIuOS0yLjgtMi45IC0xLjUgMC0yLjggMS4zLTIuOCAyLjkgMCA0LjEgMy4yIDcuNCA3LjEgNy40czcuMS0zLjMgNy4xLTcuNGMwLTYuNi01LjEtMTItMTEuNC0xMiAtNi4zIDAtMTEuNCA1LjQtMTEuNCAxMiAwIDkuMSA3IDE2LjUgMTUuNyAxNi41IDguNiAwIDE1LjctNy40IDE1LjctMTYuNSAwLTExLjYtOS0yMS0yMC0yMXMtMjAgOS40LTIwIDIxYzAgMTQuMSAxMC45IDI1LjUgMjQuMyAyNS41czI0LjMtMTEuNCAyNC4zLTI1LjVjMC0xNi42LTEyLjgtMzAtMjguNi0zMCAtMTUuOCAwLTI4LjYgMTMuNS0yOC42IDMwIDAgOS4yIDMuNCAxNy45IDkuNiAyNC40IDYuMiA2LjUgMTQuNSAxMC4xIDIzLjIgMTAuMXMxNy0zLjYgMjMuMi0xMC4xYzYuMi02LjUgOS42LTE1LjIgOS42LTI0LjQgMC0xMC40LTMuOS0yMC4yLTEwLjktMjcuNiAtNy03LjQtMTYuMy0xMS40LTI2LjMtMTEuNHMtMTkuMyA0LjEtMjYuMyAxMS40UzEzIDQxLjggMTMgNTIuMmMwIDAuOS0wLjcgMS42LTEuNSAxLjZTMTAgNTMuMSAxMCA1Mi4yYzAtMTEuMyA0LjItMjEuOSAxMS44LTI5LjkgNy42LTggMTcuNy0xMi40IDI4LjQtMTIuNCAxMC43IDAgMjAuOCA0LjQgMjguNCAxMi40IDcuNiA4IDExLjggMTguNiAxMS44IDI5LjkgMCAxMC4xLTMuNyAxOS41LTEwLjUgMjYuN0M3My4xIDg2IDY0LjEgODkuOSA1NC41IDg5Ljl6IiBmaWxsPSIjZmZmIj48YW5pbWF0ZVRyYW5zZm9ybSBhdHRyaWJ1dGVOYW1lPSJ0cmFuc2Zvcm0iIHR5cGU9InJvdGF0ZSIgZnJvbT0iMCA1MCA1MCIgdG89IjM2MCA1MCA1MCIgZHVyPSIxcyIgcmVwZWF0Q291bnQ9ImluZGVmaW5pdGUiLz48L3BhdGg+PC9zdmc+) 50% 50% no-repeat
        }
        #yohoho-buttons {
            position:absolute;
            z-index:9999;
            left:0;
            top:-45px;
            width:100%;
            text-align:left
        }
        #yohoho-buttons>li>span {
            margin-right: 2px;
            line-height: 1.42857143;
            border: 1px solid transparent;
            border-radius: 4px 4px 0 0;
        }
        #yohoho-buttons>li>span {
            position: relative;
            display: block;
            padding: 10px 15px;
        }
        #yohoho-buttons>li>span {
            border-radius: 0;
            color: #337ab7;
            text-decoration: none;
            background-color: transparent;
            cursor: pointer;
        }
        #yohoho-buttons>li.active>span, #yohoho-buttons>li.active>span:focus, #yohoho-buttons>li.active>span:hover {
            color: #555;
            cursor: default;
            background-color: #fff;
            border: 1px solid #ddd;
            border-bottom-color: transparent;
        }
        #yohoho .table {
            background-color: white;
        }
        #yohoho-torrent-player-wrapper {
            overflow-x: hidden;
            height: 555px;
        }
        #yohoho-torrent-player {
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;
            width: 848px;
            border: 0px;
            margin: 0px;
            padding: 0;
            position: relative;
            display: block;
            opacity: 0.8;
            box-sizing: border-box;
            transform: scale(1.1);
        }
        
        #yohoho-torrent-player:before {
            filter: blur(8px);
            -webkit-filter: blur(8px);
            content: "";
            z-index: -1;
            position: absolute;
            width : 100%;
            height: 100%;
            background: inherit;
        }
        #yohoho-torrent-player video {
            width: 100%;
        }
        div#yohoho-torrent-player:after {
            content: '\\A';
            position: absolute;
            width: 100%;
            height: 100%;
            top: 0;
            left: 0;
            z-index: -1;
            background: rgba(0,0,0,0.5);
        }
        .wy-tooltip {
            border: 1px solid #a5a5a5;
            color: #444;
            background: #f5f5f5;
            position: absolute;
            padding: 5px;
            text-align: left;
        }
        .wy-hide { 
            display: none;
        }
        .download-torrent {
            color: white;
        }
        .download-torrent:hover {
            text-decoration: none;
            color: #d6d5d5;
        }
        
    `;
    s = document.createElement('style');
    s.type = 'text/css';
    if (s.styleSheet) {
        s.styleSheet.cssText = css;
    } else {
        s.appendChild(document.createTextNode(css));
    }
    head.appendChild(s);

    l = document.createElement('div');
    l.setAttribute('id', 'yohoho-loading');
    yohoho.innerHTML = '';
    yohoho.appendChild(l);

    i = document.createElement('iframe');
    i.setAttribute('id', 'yohoho-iframe');
    i.setAttribute('class', 'yohoho-player');
    i.setAttribute('frameborder', '0');
    i.setAttribute('allowfullscreen', 'allowfullscreen');
    yohoho.appendChild(i);

    var torrentPlayerWrapper = document.createElement('div');
    torrentPlayerWrapper.setAttribute('id', 'yohoho-torrent-player-wrapper');
    yohoho.appendChild(torrentPlayerWrapper);

    var torrentPlayer = document.createElement('div');
    torrentPlayer.setAttribute('id', 'yohoho-torrent-player');
    torrentPlayer.setAttribute('class', 'yohoho-player');
    torrentPlayerWrapper.appendChild(torrentPlayer);

    if (parseInt(yohoho.offsetWidth)) {
        w = parseInt(yohoho.offsetWidth);
    } else if (parseInt(yohoho.parentNode.offsetWidth)) {
        w = (yohoho.parentNode.offsetWidth);
    } else {
        w = 610;
    }

    if (parseInt(yohoho.offsetHeight) && parseInt(yohoho.offsetHeight) < 370) {
        if (parseInt(yohoho.parentNode.offsetHeight) && parseInt(yohoho.parentNode.offsetHeight) >= 370) {
            h = parseInt(yohoho.parentNode.offsetHeight);
        } else {
            h = 370;
        }
    } else if (parseInt(yohoho.offsetHeight) && w / 3 < parseInt(yohoho.offsetHeight)) {
        h = parseInt(yohoho.offsetHeight);
    } else if (parseInt(yohoho.parentNode.offsetHeight) && w / 3 < parseInt(yohoho.parentNode.offsetHeight)) {
        h = parseInt(yohoho.parentNode.offsetHeight);
    } else {
        h = w / 2;
    }

    var style = 'width:' + w + 'px;height:555px;border:0;margin:0;padding:0;position:relative';
    var styleTorrentPlayer = 'width:' + w + 'px;border:0;margin:0;padding:0;position:relative';
    i.setAttribute('style', style);
    i.setAttribute('width', w);
    i.setAttribute('height', 555);
    torrentPlayer.setAttribute('style', styleTorrentPlayer);
    torrentPlayer.setAttribute('width', w);
    torrentPlayer.setAttribute('height', 555);
    yohoho.setAttribute('style', style);

    var buttons = document.createElement('ul');
    buttons.className = 'nav nav-tabs';
    buttons.setAttribute('id', 'yohoho-buttons');
    yohoho.insertBefore(buttons, yohoho.firstChild);

    var url = baseUrl + '/api/media?title=' + encodeURIComponent(options.titleorig) + '&year=' + options.year;
    $('#yohoho-torrent-player').css('background-image', 'url(' + options.poster + ')');

    httpGetAsync(url, '', function (response) {
        if (response.length > 0) {
            var option = document.createElement('li');
            var optionA = document.createElement('span');
            option.appendChild(optionA);
            optionA.innerText = 'TOLOKA';

            var that = option;

            option.onclick = (function (inThat, inTorrents, inButtons) {
                return function () {
                    showTorrentPlayer('HD', inThat, inButtons, inTorrents);
                }
            })(that, response, buttons);

            buttons.appendChild(option);

            showTorrentPlayer('HD', that, buttons, response);
            isTabSelected = true;
        }

    }, 'GET');

    setTimeout(function () {
        httpGetAsync('https://4h0y.yohoho.cc', p,
            function (players) {
                var first = true;
                var keys = options.player.split(',');
                if (/\/\/|%2F%2F/i.test(options.player)) {
                    var p = [];
                    for (var k = 0; k < keys.length; k++) {
                        var re = keys[k].match(/^(.*?)(http.*|\/\/.*)$/i);
                        if (!re || !re[1] || !re[1].trim()) continue;
                        p.push(re[1].trim());
                    }
                    keys = p.length ? p : Object.keys(players);
                }
                var j = 0;
                for (var i = 0, len = keys.length; i < len; i++) {
                    var key = keys[i].toLowerCase().trim();
                    if (players.hasOwnProperty(key) && players[key] && players[key].iframe) {
                        if (key === 'moonwalk') {
                            if (options.start_episode) {
                                var reg = options.start_episode.match(/^([a-z0-9]*?)\|([0-9]*?)\|([0-9]*?)$/i);
                                if (reg && reg.length === 4) {
                                    players[key].iframe = players[key].iframe
                                        .replace(/serial\/([a-z0-9]*?)\//i, 'serial/' + reg[1] + '/');
                                    players[key].iframe = (players[key].iframe.indexOf('?') + 1)
                                        ? players[key].iframe + '&season=' + reg[2] + '&episode=' + reg[3]
                                        : players[key].iframe + '?season=' + reg[2] + '&episode=' + reg[3]
                                }
                            }
                            if (options.start_time) {
                                players[key].iframe = (players[key].iframe.indexOf('?') + 1)
                                    ? players[key].iframe + '&start_time=' + options.start_time
                                    : players[key].iframe + '?start_time=' + options.start_time
                            }
                        }
                        players[key].quality = (players[key].quality)
                            ? players[key].quality.replace(/"/g, '\'')
                            : '';
                        players[key].translate = (players[key].translate)
                            ? players[key].translate.replace(/"/g, '\'')
                            : '';
                        var option = document.createElement('li');
                        var optionA = document.createElement('span');
                        option.appendChild(optionA);

                        var iframe = players[key].iframe;
                        var quality = players[key].quality;
                        var translate = players[key].translate;
                        var that = option;

                        option.onclick = (function (inIframe, inQuality, inTranslate, inThat) {
                            return function () {
                                showPlayer(inIframe, inQuality, inTranslate, inThat);
                            }
                        })(iframe, quality, translate, that);

                        option.dataset.iframe = players[key].iframe;
                        option.dataset.quality = players[key].quality;
                        option.dataset.translate = players[key].translate;
                        if (btns.hasOwnProperty(key) && btns[key]) {
                            var q = (players[key].quality)
                                ? (players[key].quality.toUpperCase().search(/TS|TC|SCR|CAM/gi) + 1)
                                    ? 'ЕКРАН'
                                    : (players[key].quality.toUpperCase().search(/720P/gi) + 1)
                                        ? '720P'
                                        : (players[key].quality.toUpperCase().search(/1080P/gi) + 1)
                                            ? '1080P'
                                            : players[key].quality
                                                .toUpperCase()
                                                .replace(/\s?ХОРОШЕЕ\s?|\s?СРЕДНЕЕ\s?|\s?ПЛОХОЕ\s?/gi, '')
                                : '';
                            var t = (players[key].translate)
                                ? (players[key].translate.toUpperCase().indexOf('ДУБЛ') + 1)
                                    ? 'ДУБЛЯЖ'
                                    : (players[key].translate.toUpperCase().indexOf('ПРОФ') + 1)
                                        ? 'ПРОФ.'
                                        : (players[key].translate.toUpperCase().indexOf('ЛЮБИТ') + 1)
                                            ? 'ЛЮБИТ.'
                                            : (players[key].translate.toUpperCase().indexOf('АВТОР') + 1)
                                                ? 'АВТОР.'
                                                : (players[key].translate.toUpperCase().indexOf('МНОГОГОЛ') + 1)
                                                    ? 'БАГАТОГОЛ.'
                                                    : (players[key].translate.toUpperCase().indexOf('ОДНОГОЛ') + 1)
                                                        ? 'ОДНОГОЛ.'
                                                        : (players[key].translate.toUpperCase().indexOf('ДВУХГОЛ') + 1)
                                                            ? 'ДВОХГОЛ.'
                                                            : (players[key].translate.toUpperCase().indexOf('ОРИГИНАЛ') + 1)
                                                                ? 'ОРИГІНАЛ'
                                                                : players[key].translate.toUpperCase()
                                : '';
                            j++;
                            btns[key] = btns[key]
                                .replace('{N}', j)
                                .replace('{Q}', q)
                                .replace('{T}', t)
                                .replace(/\s+/g, ' ')
                                .replace(/(^\s*)|(\s*)$/g, '');
                            btns[key] = (btns[key]) ? btns[key] : key.toUpperCase();
                            optionA.innerText = btns[key];
                        } else if (key === 'trailer') {
                            j++;
                            optionA.innerText = language.trailer.toUpperCase();
                        } else if (key === 'torrent') {
                            j++;
                            optionA.innerText = language.torrent.toUpperCase();
                        } else {
                            j++;
                            optionA.innerText = key.toUpperCase();
                        }
                        if (first && !isTabSelected) {
                            showPlayer(players[key].iframe, players[key].quality, players[key].translate, option, buttons);
                            first = false;
                        }
                        buttons.appendChild(option);
                    }
                }
                if (j < 1) {
                    var yohohoLoading = document.querySelector('#yohoho-loading');
                    yohohoLoading.style.display = 'none';
                }
            })
    }, 2000);
}

function showPlayer(iframe, quality, translate, element, buttons) {
    window.parent.postMessage({"quality": quality, "translate": translate}, "*");
    var yohohoLoading = document.querySelector('#yohoho-loading');
    yohohoLoading.style.display = 'block';
    setTimeout(function () {
        yohohoLoading.style.display = 'none';
    }, 1000);

    var player = document.querySelector('#yohoho-torrent-player');
    player.style.display = 'none';

    var yohohoIframe = document.querySelector('#yohoho-iframe');
    yohohoIframe.style.display = 'block';
    yohohoIframe.setAttribute('src', iframe);
    // yohohoIframe.setAttribute('class', '');
    if (typeof element.setAttribute === 'function') {
        var yohohoActive = document.querySelectorAll('.yohoho-active');
        if (yohohoActive) {
            for (var i = 0; i < yohohoActive.length; i++) {
                yohohoActive[i].setAttribute('class', '');
            }
        }
        element.setAttribute('class', 'active yohoho-active');
    }
    var yohohoButtons = (buttons) ? buttons : document.querySelector('#yohoho-buttons');
    if (yohohoButtons) {
        yohohoButtons.style.left = 0;
    }
}

function showTorrentPlayer(quality, element, buttons, torrents) {
    window.parent.postMessage({"quality": quality, "translate": 'ukr'}, "*");
    var yohohoLoading = document.querySelector('#yohoho-loading');
    yohohoLoading.style.display = 'block';
    setTimeout(function () {
        yohohoLoading.style.display = 'none';
    }, 500);

    var yohohoIframe = document.querySelector('#yohoho-iframe');
    yohohoIframe.style.display = 'none';

    var player = document.querySelector('#yohoho-torrent-player');
    player.style.display = 'block';
    if (typeof element.setAttribute === 'function') {
        var yohohoActive = document.querySelectorAll('.yohoho-active');
        if (yohohoActive) {
            for (var i = 0; i < yohohoActive.length; i++) {
                yohohoActive[i].setAttribute('class', '');
            }
        }
        element.setAttribute('class', 'active yohoho-active');
    }
    var yohohoButtons = (buttons) ? buttons : document.querySelector('#yohoho-buttons');

    if (yohohoButtons && yohohoButtons.style) {
        yohohoButtons.style.left = '0';
    }

    if (tolokaTabInited) {
        return;
    }

    const $wrap = $(`<div style="padding: 70px; color: white; grid-template-columns: repeat(auto-fit, minmax(250px,1fr)); display: grid; grid-template-rows: auto; grid-row-gap: 1em; grid-column-gap: 1em;"></div>`);

    torrents.forEach(t => {
        const $item = $(
            `<div style="display: flex; justify-content: space-around; align-items: center; margin-bottom: 20px; background-color: #00000040; padding: 15px 15px 15px 0; border: 1px solid;">
            <div style="width: 50%; border-right: 1px solid #ffffff82; padding-right: 30px; text-align: right;">
                <div style="font-weight: bold; font-size: 1.8rem; white-space: nowrap;">
                    ${t.quality.name}
                </div>
                <div style="font-size: 1.77rem;">
                    ` + formatBytes(t.size * 1000) + `
                </div>
            </div>
            <div style="text-align: center; padding-right: 20px">
            <div>
                <a class="download-torrent" style="cursor: pointer" title="Завантажити Torrent файл" href="${baseUrl}/api/media/${t.id}/download" target="_blank">
                    <svg width="25px" height="25px" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="download" class="svg-inline--fa fa-download fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path fill="currentColor" d="M216 0h80c13.3 0 24 10.7 24 24v168h87.7c17.8 0 26.7 21.5 14.1 34.1L269.7 378.3c-7.5 7.5-19.8 7.5-27.3 0L90.1 226.1c-12.6-12.6-3.7-34.1 14.1-34.1H192V24c0-13.3 10.7-24 24-24zm296 376v112c0 13.3-10.7 24-24 24H24c-13.3 0-24-10.7-24-24V376c0-13.3 10.7-24 24-24h146.7l49 49c20.1 20.1 52.5 20.1 72.6 0l49-49H488c13.3 0 24 10.7 24 24zm-124 88c0-11-9-20-20-20s-20 9-20 20 9 20 20 20 20-9 20-20zm64 0c0-11-9-20-20-20s-20 9-20 20 9 20 20 20 20-9 20-20z"></path></svg>
                </a>
                &nbsp;&nbsp;
                <span style="cursor: pointer" title="Копіювати Magnet посилання" class="copy-to-clipboard download-torrent" data-clipboard="${t.torrentFile.magnetLink}">
                    <svg width="25px" height="25px" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="magnet" class="svg-inline--fa fa-magnet fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path fill="currentColor" d="M164.07 148.1H12a12 12 0 0 1-12-12v-80a36 36 0 0 1 36-36h104a36 36 0 0 1 36 36v80a11.89 11.89 0 0 1-11.93 12zm347.93-12V56a36 36 0 0 0-36-36H372a36 36 0 0 0-36 36v80a12 12 0 0 0 12 12h152a11.89 11.89 0 0 0 12-11.9zm-164 44a12 12 0 0 0-12 12v52c0 128.1-160 127.9-160 0v-52a12 12 0 0 0-12-12H12.1a12 12 0 0 0-12 12.1c.1 21.4.6 40.3 0 53.3 0 150.6 136.17 246.6 256.75 246.6s255-96 255-246.7c-.6-12.8-.2-33 0-53.2a12 12 0 0 0-12-12.1z"></path></svg>
                </span>
                &nbsp;&nbsp;
                <a style="font-size: 35px; font-weight:bold; cursor: pointer" title="Перейти на толоку" class="download-torrent" href="https://toloka.to/${t.tolokaTorrentId}" target="_blank">
                    H
                </a>
            </div>
            <div style="font-size: 1.5rem; margin-top: 5px">
                <span style="color: #c5ff95">${t.torrentFile.seeders}&nbsp;&#8673;</span>
                &nbsp;&nbsp;
                <span style="color: #ff835e">&#8675;&nbsp;${t.torrentFile.leechers}</span>
            </div>
            </div>
        </div>`);
        $wrap.append($item);
    });
    $('#yohoho-torrent-player').append($wrap);
    $('.copy-to-clipboard').on('click', (e) => {
        copyToClipboard($(e.currentTarget).data('clipboard'), e);
    });

    tolokaTabInited = true;
}

function httpGetAsync(url, body, callback, method = 'POST') {
    var xmlHttp = new XMLHttpRequest();

    xmlHttp.onreadystatechange = function () {
        if (xmlHttp.readyState === 4 && xmlHttp.status === 200) {
            callback(tryParseJSON(xmlHttp.responseText));
        }
    };
    xmlHttp.open(method, url, true);
    xmlHttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xmlHttp.send(body);
}

function tryParseJSON(jsonString) {
    try {
        var o = JSON.parse(jsonString);
        if (o && typeof o === "object") {
            return o;
        }
    } catch (e) {
    }
    return {};
}

function formatBytes(bytes, decimals = 2) {
    if (bytes === 0) return '0 Bytes';

    const k = 1024;
    const dm = decimals < 0 ? 0 : decimals;
    const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

    const i = Math.floor(Math.log(bytes) / Math.log(k));

    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
}

function copyToClipboard(str, e) {
    const el = document.createElement('textarea');  // Create a <textarea> element
    el.value = str;                                 // Set its value to the string that you want copied
    el.setAttribute('readonly', '');                // Make it readonly to be tamper-proof
    el.style.position = 'absolute';
    el.style.left = '-9999px';                      // Move outside the screen to make it invisible
    document.body.appendChild(el);                  // Append the <textarea> element to the HTML document
    const selected =
        document.getSelection().rangeCount > 0        // Check if there is any content selected previously
            ? document.getSelection().getRangeAt(0)     // Store selection if found
            : false;                                    // Mark as false to know no selection existed before
    el.select();                                    // Select the <textarea> content
    document.execCommand('copy');                   // Copy - only works as a result of a user action (e.g. click events)
    document.body.removeChild(el);                  // Remove the <textarea> element
    if (selected) {                                 // If a selection existed before copying
        document.getSelection().removeAllRanges();    // Unselect everything on the HTML document
        document.getSelection().addRange(selected);   // Restore the original selection
    }

    showTooltip('Скопійовано!', e);
}

function showTooltip(str, e) {
    const delayShow = 100;
    const delayHide = 5000;

    $('<div class="wy-tooltip wy-hide"></div>').text(str).appendTo('body');

    setTimeout(function () {
        $('.wy-tooltip').removeClass('wy-hide').fadeIn('fast');
        placeTooltip(e.pageX, e.pageY);
    }, delayShow);

    setTimeout(function (e) {
        $('.wy-tooltip').addClass('wy-hide').fadeOut('fast');
    }, delayHide);

    $('body').on('mousemove', function (e) {
        placeTooltip(e.pageX, e.pageY);
    });

    function placeTooltip(CursorX, CursorY) {
        var pLeft;
        var pTop;
        var offset = 10;
        var WindowWidth = $(window).width();
        var WindowHeight = $(window).height();
        var toolTip = $('.wy-tooltip');
        var TTWidth = toolTip.width();
        var TTHeight = toolTip.height();
        if (CursorX - offset >= (WindowWidth / 4) * 3) {
            pLeft = CursorX - TTWidth - offset;
        } else {
            pLeft = CursorX + offset;
        }
        if (CursorY - offset >= (WindowHeight / 4) * 3) {
            pTop = CursorY - TTHeight - offset;
        } else {
            pTop = CursorY + offset;
        }
        $('.wy-tooltip').css({top: pTop, left: pLeft})
    }
}
